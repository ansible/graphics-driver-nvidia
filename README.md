# NVIDIA graphics driver

Host `asks2` has a Quadro P620 graphics card (NVIDIA Corporation GP107GL).

We are using the proprietary Nvidia drivers,
and using the ["Graphics Drivers" team PPA](https://launchpad.net/~graphics-drivers/+archive/ubuntu/ppa)
for older Ubuntu releases (up to 18.04).
In more recent Ubuntu releases, the Nvidia drivers are included in the apt repos.



## Functionality

It's all controlled from the `xorg.conf` file, which I generated using the `nvidia-settings` GUI tool,
and then saved the config file into this playbook.

+ Rotation of the portrait monitors, including rotation of the login screen


## Driver versions (for Quadro cards)

> **QNF** (Quadro New Feature) drivers provide users the flexibility to try
> new features made available outside the launch cycle of longer-lived ODE branches.
> QNF drivers are tested for workstation environments and are recommended only 
> for those users who need specific features mentioned in the release highlights
> or release notes.

> **ODE** (Optimal Driver for Enterprise) branches are designed and tested to 
> provide long-term stability and availability,
> making drivers from these branches ideal for enterprise customers and other users 
> requiring application and hardware certification. 

> **PB** (Production Branch) drivers are designed and tested to provide long-term
> stability and availability, making these drivers ideal for enterprise customers
> and other users who require application and hardware certification from ISVs
> and OEMs respectively.

+ Release 415 is a QNF driver (release date 2018-10-12).
+ Release 430 is an ODE branch release.
+ Release 450 is an ODE branch release (2020-06-24).
+ Release 455 is a QNF driver.
+ Release 460 is a PB release (2020-12-15).
+ [Release 470 is a PB release (2021-08-10)](https://www.nvidia.com/download/driverResults.aspx/179687/en-us/).
+ [Release 510 is a PB release (2022-05-16)](https://www.nvidia.com/download/driverResults.aspx/189362/en-us/).


## Diagnostic utilities, with expected/normal output


```
taha@asks2:~
$ sudo lspci -nnk | grep -iA3 vga
10:00.0 VGA compatible controller [0300]: NVIDIA Corporation GP107GL [Quadro P620] [10de:1cb6] (rev a1)
	Subsystem: NVIDIA Corporation Device [10de:1264]
	Kernel driver in use: nvidia
	Kernel modules: nvidiafb, nouveau, nvidia_drm, nvidia
```

```
$ nvidia-smi 
Tue Jun 29 17:32:28 2021       
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 460.84       Driver Version: 460.84       CUDA Version: 11.2     |
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|                               |                      |               MIG M. |
|===============================+======================+======================|
|   0  Quadro P620         Off  | 00000000:10:00.0  On |                  N/A |
| 35%   48C    P0    N/A /  N/A |    377MiB /  1991MiB |     10%      Default |
|                               |                      |                  N/A |
+-------------------------------+----------------------+----------------------+
                                                                               
+-----------------------------------------------------------------------------+
| Processes:                                                                  |
|  GPU   GI   CI        PID   Type   Process name                  GPU Memory |
|        ID   ID                                                   Usage      |
|=============================================================================|
|    0   N/A  N/A      1542      G   /usr/lib/xorg/Xorg                311MiB |
|    0   N/A  N/A      2022      G   /usr/bin/nextcloud                  1MiB |
+-----------------------------------------------------------------------------+
```

```
$ glxinfo | grep -i opengl
OpenGL vendor string: NVIDIA Corporation
OpenGL renderer string: Quadro P620/PCIe/SSE2
OpenGL core profile version string: 4.6.0 NVIDIA 460.84
OpenGL core profile shading language version string: 4.60 NVIDIA
OpenGL core profile context flags: (none)
OpenGL core profile profile mask: core profile
OpenGL core profile extensions:
OpenGL version string: 4.6.0 NVIDIA 460.84
OpenGL shading language version string: 4.60 NVIDIA
OpenGL context flags: (none)
OpenGL profile mask: (none)
OpenGL extensions:
OpenGL ES profile version string: OpenGL ES 3.2 NVIDIA 460.84
OpenGL ES profile shading language version string: OpenGL ES GLSL ES 3.20
OpenGL ES profile extensions:
```

```
$ ubuntu-drivers devices
WARNING:root:_pkg_get_support nvidia-driver-390: package has invalid Support Legacyheader, cannot determine support level
== /sys/devices/pci0000:00/0000:00:01.1/0000:10:00.0 ==
modalias : pci:v000010DEd00001CB6sv000010DEsd00001264bc03sc00i00
vendor   : NVIDIA Corporation
driver   : nvidia-driver-460-server - distro non-free
driver   : nvidia-driver-460 - third-party non-free recommended
driver   : nvidia-driver-465 - third-party non-free
driver   : nvidia-driver-390 - third-party non-free
driver   : xserver-xorg-video-nouveau - distro free builtin
```

(I don't understand why ubuntu-drivers still lists version 390).


```
$ hwinfo --gfxcard
31: PCI 1000.0: 0300 VGA compatible controller (VGA)            
  [Created at pci.378]
  Unique ID: h0vA.SulT1s0Hu23
  Parent ID: mnDB.fA+tdbAkMSD
  SysFS ID: /devices/pci0000:00/0000:00:01.1/0000:10:00.0
  SysFS BusID: 0000:10:00.0
  Hardware Class: graphics card
  Model: "nVidia VGA compatible controller"
  Vendor: pci 0x10de "nVidia Corporation"
  Device: pci 0x1cb6 
  SubVendor: pci 0x10de "nVidia Corporation"
  SubDevice: pci 0x1264 
  Revision: 0xa1
  Driver: "nvidia"
  Driver Modules: "nvidia"
  Memory Range: 0xf6000000-0xf6ffffff (rw,non-prefetchable)
  Memory Range: 0xe0000000-0xefffffff (ro,non-prefetchable)
  Memory Range: 0xf0000000-0xf1ffffff (ro,non-prefetchable)
  I/O Ports: 0xf000-0xf07f (rw)
  Memory Range: 0x000c0000-0x000dffff (rw,non-prefetchable,disabled)
  IRQ: 84 (107891258 events)
  Module Alias: "pci:v000010DEd00001CB6sv000010DEsd00001264bc03sc00i00"
  Driver Info #0:
    Driver Status: nvidiafb is not active
    Driver Activation Cmd: "modprobe nvidiafb"
  Driver Info #1:
    Driver Status: nouveau is not active
    Driver Activation Cmd: "modprobe nouveau"
  Driver Info #2:
    Driver Status: nvidia_drm is active
    Driver Activation Cmd: "modprobe nvidia_drm"
  Driver Info #3:
    Driver Status: nvidia is active
    Driver Activation Cmd: "modprobe nvidia"
  Config Status: cfg=new, avail=yes, need=no, active=unknown
  Attached to: #28 (PCI bridge)

Primary display adapter: #31
```

```
$ cat /proc/driver/nvidia/version
NVRM version: NVIDIA UNIX x86_64 Kernel Module  460.84  Wed May 26 20:14:59 UTC 2021
GCC version:  gcc version 7.5.0 (Ubuntu 7.5.0-3ubuntu1~18.04) 
```

List nvidia's kernel modules:
```
$ lsmod | grep nvidia
nvidia_uvm            987136  0
nvidia_drm             53248  3
nvidia_modeset       1228800  5 nvidia_drm
nvidia              34148352  178 nvidia_uvm,nvidia_modeset
drm_kms_helper        188416  1 nvidia_drm
drm                   491520  6 drm_kms_helper,nvidia_drm
```

```
$ find /lib/modules | grep nvidia
/lib/modules/5.4.0-74-generic/kernel/drivers/usb/typec/altmodes/typec_nvidia.ko
/lib/modules/5.4.0-74-generic/kernel/drivers/net/ethernet/nvidia
/lib/modules/5.4.0-74-generic/kernel/drivers/net/ethernet/nvidia/forcedeth.ko
/lib/modules/5.4.0-74-generic/kernel/drivers/i2c/busses/i2c-nvidia-gpu.ko
/lib/modules/5.4.0-74-generic/kernel/drivers/video/fbdev/nvidia
/lib/modules/5.4.0-74-generic/kernel/drivers/video/fbdev/nvidia/nvidiafb.ko
/lib/modules/5.4.0-74-generic/updates/dkms/nvidia.ko
/lib/modules/5.4.0-74-generic/updates/dkms/nvidia-drm.ko
/lib/modules/5.4.0-74-generic/updates/dkms/nvidia-uvm.ko
/lib/modules/5.4.0-74-generic/updates/dkms/nvidia-modeset.ko
/lib/modules/4.15.0-147-generic/kernel/drivers/net/ethernet/nvidia
/lib/modules/4.15.0-147-generic/kernel/drivers/net/ethernet/nvidia/forcedeth.ko
/lib/modules/4.15.0-147-generic/kernel/drivers/video/fbdev/nvidia
/lib/modules/4.15.0-147-generic/kernel/drivers/video/fbdev/nvidia/nvidiafb.ko
/lib/modules/5.4.0-77-generic/kernel/drivers/usb/typec/altmodes/typec_nvidia.ko
/lib/modules/5.4.0-77-generic/kernel/drivers/net/ethernet/nvidia
/lib/modules/5.4.0-77-generic/kernel/drivers/net/ethernet/nvidia/forcedeth.ko
/lib/modules/5.4.0-77-generic/kernel/drivers/i2c/busses/i2c-nvidia-gpu.ko
/lib/modules/5.4.0-77-generic/kernel/drivers/video/fbdev/nvidia
/lib/modules/5.4.0-77-generic/kernel/drivers/video/fbdev/nvidia/nvidiafb.ko
/lib/modules/5.4.0-77-generic/updates/dkms/nvidia.ko
/lib/modules/5.4.0-77-generic/updates/dkms/nvidia-drm.ko
/lib/modules/5.4.0-77-generic/updates/dkms/nvidia-uvm.ko
/lib/modules/5.4.0-77-generic/updates/dkms/nvidia-modeset.ko
```

```
$ modinfo nvidia
filename:       /lib/modules/5.4.0-77-generic/updates/dkms/nvidia.ko
alias:          char-major-195-*
version:        460.84
supported:      external
license:        NVIDIA
srcversion:     EA32CEBBA576FA0CDF3786B
alias:          pci:v000010DEd*sv*sd*bc03sc02i00*
alias:          pci:v000010DEd*sv*sd*bc03sc00i00*
depends:        
retpoline:      Y
name:           nvidia
vermagic:       5.4.0-77-generic SMP mod_unload modversions 
signat:         PKCS#7
signer:         
sig_key:        
sig_hashalgo:   md4
parm:           NvSwitchRegDwords:NvSwitch regkey (charp)
parm:           NvSwitchBlacklist:NvSwitchBlacklist=uuid[,uuid...] (charp)
parm:           nv_cap_enable_devfs:Enable (1) or disable (0) nv-caps devfs support. Default: 1 (int)
parm:           NVreg_ResmanDebugLevel:int
parm:           NVreg_RmLogonRC:int
parm:           NVreg_ModifyDeviceFiles:int
parm:           NVreg_DeviceFileUID:int
parm:           NVreg_DeviceFileGID:int
parm:           NVreg_DeviceFileMode:int
parm:           NVreg_InitializeSystemMemoryAllocations:int
parm:           NVreg_UsePageAttributeTable:int
parm:           NVreg_RegisterForACPIEvents:int
parm:           NVreg_EnablePCIeGen3:int
parm:           NVreg_EnableMSI:int
parm:           NVreg_TCEBypassMode:int
parm:           NVreg_EnableStreamMemOPs:int
parm:           NVreg_EnableBacklightHandler:int
parm:           NVreg_RestrictProfilingToAdminUsers:int
parm:           NVreg_PreserveVideoMemoryAllocations:int
parm:           NVreg_EnableS0ixPowerManagement:int
parm:           NVreg_S0ixPowerManagementVideoMemoryThreshold:int
parm:           NVreg_DynamicPowerManagement:int
parm:           NVreg_DynamicPowerManagementVideoMemoryThreshold:int
parm:           NVreg_EnableUserNUMAManagement:int
parm:           NVreg_MemoryPoolSize:int
parm:           NVreg_KMallocHeapMaxSize:int
parm:           NVreg_VMallocHeapMaxSize:int
parm:           NVreg_IgnoreMMIOCheck:int
parm:           NVreg_NvLinkDisable:int
parm:           NVreg_EnablePCIERelaxedOrderingMode:int
parm:           NVreg_RegisterPCIDriver:int
parm:           NVreg_RegistryDwords:charp
parm:           NVreg_RegistryDwordsPerDevice:charp
parm:           NVreg_RmMsg:charp
parm:           NVreg_GpuBlacklist:charp
parm:           NVreg_TemporaryFilePath:charp
```


## Refs

+ https://www.pny.com/nvidia-quadro-p620
+ [Ubuntu certified component: nVidia Quadro P620](https://ubuntu.com/certified/component/715)
+ [Ubuntu certified component: nVidia GP107GL (Quadro P620)](https://ubuntu.com/certified/component/382)
+ https://launchpad.net/~graphics-drivers/+archive/ubuntu/ppa
+ https://linoxide.com/linux-how-to/how-to-install-nvidia-driver-on-ubuntu
+ https://reddit.com/r/linuxquestions/comments/ttia4s/nvidia_driver_470_vs_510
